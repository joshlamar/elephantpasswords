@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="container">
            <div class="column is-half is-offset-one-quarter box">
                <form role="form" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}

                    <label for="first_name">First name</label>
                    <div class="field">
                        <p class="control">
                            <input id="first_name" type="text" class="input{{ $errors->has('first_name') ? ' is-danger' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus>
                        </p>
                    </div>
                    @if ($errors->has('first_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                    @endif

                    <label for="last_name" class="col-md-4 control-label">Last name</label>
                    <div class="field">
                        <p class="control">
                            <input id="last_name" type="text" class="input{{ $errors->has('last_name') ? ' is-danger' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus>
                        </p>
                    </div>
                    @if ($errors->has('last_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                    @endif

                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                    <div class="field">
                        <p class="control">
                            <input id="email" type="email" class="input{{ $errors->has('email') ? ' is-danger' : '' }}" name="email" value="{{ old('email') }}" required>
                        </p>
                    </div>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                    <label for="password" class="col-md-4 control-label">Password</label>
                    <div class="field">
                        <p class="control">
                            <input id="password" type="password" class="input{{ $errors->has('password') ? ' is-danger' : '' }}" name="password" required>
                        </p>
                    </div>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif

                    <label for="password-confirm">Confirm Password</label>

                    <div class="field">
                        <p class="control">
                            <input id="password-confirm" type="password" class="input{{ $errors->has('password_confirmation') ? ' is-danger' : '' }}" name="password_confirmation" required>
                        </p>
                    </div>

                    <div class="field">
                        <p class="control">
                            <button type="submit" class="button is-primary">
                                Register
                            </button>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
