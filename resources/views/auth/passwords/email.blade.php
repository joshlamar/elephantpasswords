@extends('layouts.app')

@section('content')
<div class="container">
    <div class="column is-half is-offset-one-quarter box" style="margin-top:1em">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}

            <div class="field">
                <label for="email">E-Mail Address</label>

                <input id="email" type="email" class="input{{ $errors->has('email') ? ' is-danger' : '' }}" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <button type="submit" class="button">
                Send Password Reset Link
            </button>
        </form>
    </div>
</div>
@endsection
