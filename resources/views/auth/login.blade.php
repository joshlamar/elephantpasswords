@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="container">
            <div class="column is-half is-offset-one-quarter box">
                <form role="form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <label for="email">E-Mail Address</label>

                    <div class="field">
                        <p class="control">
                            <input id="email" type="email" class="input{{ $errors->has('email') ? ' is-danger' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                        </p>
                    </div>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                    <label for="password">Password</label>
                    <div class="field">
                        <p class="control">
                            <input id="password" type="password" class="input{{ $errors->has('password') ? ' is-danger' : '' }}" name="password" required>
                        </p>
                    </div>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif

                    <div class="field">
                        <p class="control">
                            <label class="checkbox">
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                            </label>
                        </p>
                    </div>

                    <div class="field is-grouped">
                        <p class="control">
                            <button type="submit" class="button is-primary">
                                Login
                            </button>
                        </p>
                        <p class="control">
                            <button class="button is-link" href="{{ route('password.request') }}">Forgot Your Password?</button>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
