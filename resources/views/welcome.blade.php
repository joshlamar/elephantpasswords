@extends('layouts.app')

@section('content')
    <section class="hero is-primary is-bold">
        <div class="hero-body">
            <div class="container">
                <div class="columns" style="padding-left:.75rem">
                    <div class="column is-half">
                        @if(!Auth::check())
                            <h2 class="lead-text">
                                Store and manage your passwords in one place with Elephant Passwords.
                            </h2>
                            <ul class="features">
                                <li class="feature"><span class="emoji is-large">🤔</span><p class="feature__text"><strong>Never waste brain power</strong> on trying to remember your password.</p></li>
                                <li class="feature"><span class="emoji is-large">🔎</span><p class="feature__text"><strong>Passwords are cross-checked</strong> to make sure all of yours are unique.</p></li>
                                <li class="feature"><span class="emoji is-large">🐘</span><p class="feature__text">Have confidence in knowing that <strong>an elephant never forgets</strong>.</p></li>
                            </ul>
                        @else

                            <h2 class="title">
                                Save a new password
                            </h2>
                            <form role="form" method="POST" action="/passwords" autocomplete="off">
                                {{ csrf_field() }}
                                <div class="field">
                                    <p class="control">
                                        <input class="input is-medium" value="{{ session('name') }}" type="text" name="name" placeholder="Application name" />
                                    </p>
                                </div>


                                <div class="field">
                                    <p class="control">
                                        <input class="input is-medium" value="{{ session('url') }}" type="text" name="url" placeholder="URL" />
                                    </p>
                                </div>


                                <div class="field">
                                    <p class="control">
                                        <input style="display:none" type="text" name="username" />
                                        <input class="input is-medium" type="text" value="{{ session('username') }}" name="username" placeholder="Login or username" />
                                    </p>
                                </div>


                                <div class="field">
                                    <p class="control">
                                        <input style="display:none" type="password" name="password" />
                                        <input class="input is-medium" type="password" {{ session('error') ? 'autofocus="autofocus"' : '' }} name="password" placeholder="Enter a password" required />
                                    </p>
                                    @if (session('error'))
                                        <notification error="{{ session('error') }}"></notification>
                                    @endif
                                </div>

                                <div class="field">
                                    <div class="control">
                                        <input type="text" class="input is-medium" value="{{ session('description') }}" name="description" placeholder="Explain what this password is for" />
                                    </div>
                                </div>

                                <div class="field">
                                    <div class="control">
                                        <button class="button is-light is-outlined">
                                        <span class="icon is-small">
                                          <i class="fa fa-plus"></i>
                                        </span>
                                            <span>Save Password</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        @endif
                    </div>

                    @if(!Auth::check())
                        <div class="column is-half">
                            <h3 class="title">Sign up</h3>
                            <p class="subtitle">It's free and always will be.</p>
                            <form role="form" method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}


                                <div class="columns">
                                    <div class="column is-half">
                                        <label for="first_name">First name</label>
                                        <div class="field">
                                            <p class="control">
                                                <input id="first_name" type="text" class="input{{ $errors->has('first_name') ? ' is-danger' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus>
                                            </p>
                                        </div>
                                        @if ($errors->has('first_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="column is-half">
                                        <label for="last_name" class="col-md-4 control-label">Last name</label>
                                        <div class="field">
                                            <p class="control">
                                                <input id="last_name" type="text" class="input{{ $errors->has('last_name') ? ' is-danger' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus>
                                            </p>
                                        </div>
                                        @if ($errors->has('last_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>




                                <label for="email" class="col-md-4 control-label">Email</label>
                                <div class="field">
                                    <p class="control">
                                        <input id="email" type="email" class="input{{ $errors->has('email') ? ' is-danger' : '' }}" name="email" value="{{ old('email') }}" required>
                                    </p>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                                <label for="password" class="col-md-4 control-label">Password</label>
                                <div class="field">
                                    <p class="control">
                                        <input id="password" type="password" class="input{{ $errors->has('password') ? ' is-danger' : '' }}" name="password" required>
                                    </p>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                                <label for="password-confirm">Confirm Password</label>

                                <div class="field">
                                    <p class="control">
                                        <input id="password-confirm" type="password" class="input{{ $errors->has('password_confirmation') ? ' is-danger' : '' }}" name="password_confirmation" required>
                                    </p>
                                </div>

                                <div class="field">
                                    <p class="control">
                                        <button type="submit" class="button is-outlined is-white">
                                            Create Account
                                        </button>
                                    </p>
                                </div>
                            </form>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    @if(Auth::check())
        <section class="section">
            <div class="columns">
                @foreach(Auth::user()->passwords as $index => $password)
                    @if($index % 3 == 0)
                        </div><div class="columns">
                    @endif
                    <card :card="{{ $password->toJson() }}"></card>
                @endforeach
            </div>
        </section>
    @endif
@endsection