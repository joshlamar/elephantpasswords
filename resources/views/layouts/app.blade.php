<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/png" href="/favicon.png" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Elephant Passwords') }}</title>
    <meta name="description" content="Elephant Passwords is a free and secure password manager. An Elephant never forgets."/>

    <!-- Styles -->
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
        <div class="container">
            <nav class="nav">
                <div class="nav-left">
                    <!-- Branding Image -->
                    <h1 class="nav-item">
                        <a href="{{ url('/') }}" class="title">
                            {{ config('app.name', 'Elephant Passwords') }}
                        </a>
                    </h1>
                </div>

                <!-- This "nav-toggle" hamburger menu is only visible on mobile -->
                <!-- You need JavaScript to toggle the "is-active" class on "nav-menu" -->
                <span class="nav-toggle">
                    <span></span>
                    <span></span>
                    <span></span>
                  </span>

                <!-- This "nav-menu" is hidden on mobile -->
                <!-- Add the modifier "is-active" to display it on mobile -->
                <div class="nav-right nav-menu">
                    @if (Auth::guest())
                        <form role="form" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}

                            <div class="columns" style="padding:1em 0">
                                <div class="column is-one-third" style="text-align:left">
                                    <label for="email">Email</label>

                                    <div class="field">
                                        <p class="control">
                                            <input id="email" type="email" class="input{{ $errors->has('email') ? ' is-danger' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                        </p>
                                    </div>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="column is-one-third" style="text-align:left">
                                    <label for="password">Password</label>
                                    <div class="field" style="margin-bottom: 0">
                                        <p class="control">
                                            <input id="password" type="password" class="input{{ $errors->has('password') ? ' is-danger' : '' }}" name="password" required>
                                        </p>
                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                    <!--<p class="control" style="text-align:left">
                                        <a class="button is-link" href="{{ route('password.request') }}">Forgot Password?</a>
                                    </p>-->
                                    <input type="hidden" name="remember" value="true" />
                                </div>
                                <div class="column is-one-fourth">
                                    <p class="control">
                                        <button type="submit" class="button is-primary" style="margin-top:1.5em">
                                            Login
                                        </button>
                                    </p>
                                </div>
                            </div>
                        </form>

                    @else
                        <a class="nav-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    @endif
                </div>
            </nav>
        </div>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
