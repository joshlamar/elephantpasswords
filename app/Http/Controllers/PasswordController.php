<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Crypt;
use Validator;
use App\Password;
use Auth;

class PasswordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Auth::user()->passwords;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $password = $request->all();
        $password["user_id"] = Auth::id();

        // loop through all the users passwords and make sure none are the same
        foreach(Auth::user()->passwords as $storedPassword) {
            if($storedPassword['decryptedPassword'] == $password['password']) {
                // create an error message
                unset($password['_token']);
                $password['error'] = 'This password is already in use by ' . ($storedPassword['name'] ? : 'another application.' );
                return redirect('/')->with($password);
            }
        }


        $password["password"] = Crypt::encryptString($password["password"]);

        Password::create($password);
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $password = $request->all();

        if (Password::withTrashed()->findOrFail($id)->trashed()) {
            Password::withTrashed()->findOrFail($id)->restore();
        }

        // loop through all the users passwords and make sure none are the same
        foreach(Auth::user()->passwords as $storedPassword) {
            if($storedPassword['decryptedPassword'] == $password['decryptedPassword'] && $storedPassword["id"] != $password["id"]) {

                // create an error message
                unset($password['_token']);
                $password['error'] = 'This password is already in use by ' . ($storedPassword['name'] ? : 'another application.' );
                return $password;
            }
        }

        $password["password"] = Crypt::encryptString($password["decryptedPassword"]);

        Password::findOrFail($id)->update($password);
        return "updated";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Password::findOrFail($id)->delete();
        return "deleted";

    }
}
